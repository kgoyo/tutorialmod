using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace TutorialMod.Items {
    public class TutorialItem : ModItem {
        public override void SetStaticDefaults() {
            Tooltip.SetDefault("Mod item");
        }

        public override void SetDefaults() {
            item.width = 20;
            item.height = 20;
            item.maxStack = 999;
            item.value = 100;
            item.rare = 0;
        }
    }
}